# Read Me

## IDE Configuration

If you use IntelliJ Idea
- Choose "Edit configurations..." from the dropdown next to the play button.
- Press the "+" button in the upper left-hand corner.
- Choose to create a maven configuration.
- Name it.
- Add spring-boot:run to the "Command line" entry.
- Optionally, make it "Share"d or "Single instance only".