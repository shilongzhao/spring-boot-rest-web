package it.dreamhack.model;

import javax.persistence.*;

/**
 * author: zhaoshilong
 * date: 28/03/2017
 */
@Entity
public class User {
    @Id @GeneratedValue(strategy= GenerationType.AUTO)
    private long userId;

    private String name;

    protected User() {}

    public User(String name) {
        this.name = name;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long uesrId) {
        this.userId = uesrId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String toString() {
        return String.format("Customer[userId = %d, name = %s]", userId, name);
    }
}
