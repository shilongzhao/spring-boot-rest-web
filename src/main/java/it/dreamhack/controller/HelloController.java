package it.dreamhack.controller;

import it.dreamhack.model.User;
import it.dreamhack.service.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


/**
 * author: zhaoshilong
 * date: 28/03/2017
 *
 * if use @RestController, it will not return HTTP page, it serves JSON data instead
 */
@Controller
public class HelloController {

    @Autowired
    private UserRepository userRepository;

    @RequestMapping("/")
    public String index() {
        return "greeting";
    }

    @RequestMapping(value = "/getUser/{id}", method = RequestMethod.GET)
    public String getUserInfo(@PathVariable(value = "id") Long id, ModelMap modelMap) {
        User user = userRepository.findByUserId(id);
        if(user == null) return "error";
        modelMap.addAttribute("username", user.getName());
        return "user";
    }
}
