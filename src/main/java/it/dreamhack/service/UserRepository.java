package it.dreamhack.service;

import it.dreamhack.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * author: zhaoshilong
 * date: 28/03/2017
 */
@Repository
public interface UserRepository extends CrudRepository<User, Long>{
    User findByUserId(Long id);
}
